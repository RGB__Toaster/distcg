package me.rgbtoast.distcg.bukkit;

import lombok.Getter;
import me.rgbtoast.api.RGBAPI;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.popcraft.chunky.api.ChunkyAPI;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public final class DistCG extends JavaPlugin {

    @Getter
    private static ChunkyAPI chunky; // TODO: Remove from DistCG.java, should only be needed in Client classes instead. See: master & onEnable() func
    @Getter
    private static Logger pluginLogger;
    @Getter
    private static boolean isMaster, isClient;
    @Getter
    private static final List<String> roles = new ArrayList<>();

    @Override
    public void onEnable() {
        pluginLogger = getSLF4JLogger();
        chunky = null;
        initConfig();

        if (isMaster) roles.add("master");
        if (isClient) roles.add("client");

        // Disable Client-capabilities if ChunkyAPI is missing.
        if (roles.contains("client") && this.getServer().getPluginManager().getPlugin("Chunky") == null ){
            setClient(false);
            pluginLogger.warn("No Generation capabilities present, disabling `client` role!");
        } else chunky = Bukkit.getServer().getServicesManager().load(ChunkyAPI.class);
        pluginLogger.info("Registered " + roles.size() + " role(s): " + roles);

        if (isMaster) initMaster();
        if (isClient) initClient();

        pluginLogger.debug("Registering commands");
        RGBAPI.getAPI().registerCommandPackage("me.rgbtoast.distcg.bukkit.commands", this);

    }

    @Override
    public void onDisable(){
        //Todo: alert another nodes you're shutting down.
        // Aka, send remaking mca files to master etc
    }

    // Todo: redo the whole config system with a new lib? // yes
    private void initConfig() {
        String configName = "config.toml";
        pluginLogger.debug("Loading " + configName);
        try {
            if (new File(getDataFolder() + configName).exists()) {
                DistCGConfig.setMainConfig(new DistCGConfig(new File(getDataFolder() + configName)));
            } else {
                //TODO: Print out new Private API token if no config existed?
                DistCGConfig.setMainConfig(new DistCGConfig(getResource(configName)));
                pluginLogger.info("Generated a new DistCG `" + configName + "` file!");

                pluginLogger.info("-------------------------------------------------");
                pluginLogger.info("SECRET CLIENT TOKEN: " + DistCGConfig.getString("general.clientToken"));
                pluginLogger.info("SECRET MASTER TOKEN: " + DistCGConfig.getString("general.masterToken"));
                pluginLogger.info("-------------------------------------------------");
            }
        } catch (IOException e) {
            pluginLogger.error("FAILED LOADING + " + configName + ", exiting!", e);
            onDisable();
        }
    }

    private void initMaster(){
        // new MasterNode(ip,port,[...])....
    }
    private void initClient(){
        // new ClientNode(ip,port,[...])....
    }

    void setMaster(boolean isMaster) {
        this.isMaster = isMaster;

        if (isMaster) roles.add("master");

        else if (roles.contains("master")) {
            roles.remove("master");
        }

        pluginLogger.debug("Setting isMaster: " + isMaster);
    }

    void setClient(boolean isClient) {
        this.isClient = isClient;

        if (isClient) roles.add("client");

        else if (roles.contains("client")) {
            roles.remove("client");
        }

        pluginLogger.debug("Setting isClient: " + isClient);
    }

}
