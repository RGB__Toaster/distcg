package me.rgbtoast.distcg.bukkit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.tomlj.Toml;
import org.tomlj.TomlArray;
import org.tomlj.TomlParseResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@EqualsAndHashCode
@ToString
@Getter
public class DistCGConfig {

    private final TomlParseResult config;
    private static DistCGConfig mainConfig;
    private static boolean debug;

    public DistCGConfig(File configFile) throws IOException {
        this.config = Toml.parse(configFile.toPath());
        debug = config.getBoolean("misc.debug");
    }

    public DistCGConfig(InputStream data) throws IOException {
        this.config = Toml.parse(data);
        debug = config.getBoolean("misc.debug");
    }

    public static void setMainConfig(DistCGConfig config) {
        mainConfig = config;
    }

    public static String getString(String key) {
        return mainConfig.getConfig().getString(key);
    }

    public static int getInt(String key) {
        return (int) mainConfig.getConfig().get(key);
    }

    public static double getDouble(String key) {
        return mainConfig.getConfig().getDouble(key);
    }

    public static boolean getBoolean(String key) {
        return mainConfig.getConfig().getBoolean(key);
    }

    public static String[] getStringArray(String key) {
        TomlArray tomlArray = mainConfig.getConfig().getArray(key);

        if (tomlArray != null) {
            String[] stringArray = new String[tomlArray.size()];

            for (int i = 0; i < tomlArray.size(); i++) {
                stringArray[i] = tomlArray.getString(i);
            }

            return stringArray;
        } else {
            return new String[0]; // Or handle the case where the array is not found as needed
        }
    }

}
