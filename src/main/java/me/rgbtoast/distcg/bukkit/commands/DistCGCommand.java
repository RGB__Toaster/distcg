package me.rgbtoast.distcg.bukkit.commands;

import me.rgbtoast.api.command.Command;
import me.rgbtoast.api.command.RGBCommand;
import me.rgbtoast.distcg.bukkit.DistCGConfig;
import me.rgbtoast.distcg.bukkit.DistCG;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
@Command(name = "distcg", reqPlayer = false)
public class DistCGCommand extends RGBCommand {

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(args.length >= 1)) { s.sendMessage(RED + "Must provide at least 1 argument"); return;};

        switch (args[0]) {
            case "status" -> {
                // Todo: Global -> Global Task progress per client.
                DistCG.getRoles().forEach((role) -> {
                    s.sendMessage(role.toUpperCase() + ": " + DistCGConfig.getBoolean(role + ".enabled")
                            + " | " + DistCGConfig.getString(role + ".listenAddress") + DistCGConfig.getString(role + ".port"));
                });

            }

            case "toggle" -> {
                // Todo: Master -> Toggle handing out tasks. Client -> Toggle accepting tasks
            }

            case "tasks" -> {
                // TODO: Return list of global tasks with node-assigment.
            }

            default -> s.sendMessage(RED + args[0] + " is not an valid argument");
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        return Arrays.asList("status", "toggle", "tasks");
        /* TODO: Implement this design.
        status: Current status of the DistCG client/master -> accepting jobs, number of clients, etc
        toggle: Weather to accept new tasks
        tasks: lists of current tasks
        */
    }

}
