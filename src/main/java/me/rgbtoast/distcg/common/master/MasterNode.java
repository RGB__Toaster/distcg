package me.rgbtoast.distcg.common.master;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import me.rgbtoast.distcg.common.client.ClientNode;
import me.rgbtoast.distcg.common.utils.Task;
import org.popcraft.chunky.api.ChunkyAPI;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

@ToString
@EqualsAndHashCode
@Getter
public class MasterNode {

    private final ChunkyAPI chunkyAPI;

    private final List<ClientNode> clients = new ArrayList<>();
    private final short port;
    private final InetAddress ip;

    public MasterNode(short port, InetAddress ip, ChunkyAPI chunkyAPI) {
        this.port = port;
        this.ip = ip;
        this.chunkyAPI = chunkyAPI;
    }

    public boolean addNode(ClientNode node) {
        return clients.add(node);
    }

    public boolean removeNode(ClientNode node) {
        return clients.remove(node);
    }

    public void sendTaskToClients(Task task) {
        if (clients.isEmpty()) {
            LoggerFactory.getLogger(getClass()).error("No clients are connected to the network.");
        }

        List<Task> tasks = new ArrayList<>();

        // Split tasks only by X-axis, leave them gen to their full y length.
        double radiusXPerNode = clients.size() / task.getRadiusX();

        for (ClientNode client : clients) {
            //task.setRadiusX(currentRadius);
            client.startTask(task);
        }
    }

}
