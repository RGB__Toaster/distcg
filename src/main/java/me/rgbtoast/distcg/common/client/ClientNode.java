package me.rgbtoast.distcg.common.client;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import me.rgbtoast.distcg.common.utils.Task;
import org.popcraft.chunky.api.ChunkyAPI;

import java.net.InetAddress;

@ToString
@EqualsAndHashCode
@Getter
public class ClientNode {

    private final ChunkyAPI chunkyAPI;

    private static final String CHUNKY_SHAPE = "square";
    private static final String CHUNKY_PATTERN = "loop";

    private final short port;
    private final InetAddress listenAddress;

    @Setter
    private boolean isRunning = false; // TODO: change to is running function

    public ClientNode(short port, InetAddress listenAddress, ChunkyAPI chunkyAPI) {
        this.port = port;
        this.listenAddress = listenAddress;
        this.chunkyAPI = chunkyAPI;
    }


    /* TODO
    private static boolean isRunning(){
        boolean isRunning;
        for (Task : tasksList) {
            chunky.isRunning()
        }
    }
     */

    public boolean startTask(Task task) {
        isRunning = true;

        return chunkyAPI.startTask(
                task.getWorldName(),
                CHUNKY_SHAPE,
                task.getCenterX(),
                task.getCenterZ(),
                task.getRadiusX(),
                task.getRadiusZ(),
                CHUNKY_PATTERN
        );

        //TODO: Inform other nodes about progress / start.
    }

    public boolean continueTask(Task task) {
        isRunning = true; //TODO: deprecate

        return chunkyAPI.continueTask(task.getWorldName());
    }

    public boolean cancelTask(Task task) {
        isRunning = false; //TODO: deprecate

        return chunkyAPI.cancelTask(task.getWorldName());
    }

    public boolean pauseTask(Task task) {
        isRunning = false; //TODO: deprecate

        return chunkyAPI.pauseTask(task.getWorldName());
    }


}
