package me.rgbtoast.distcg.common.utils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
@Getter
@Setter
public class Task {

    private final double radiusX, radiusZ;
    private final double centerX, centerZ;
    private final String worldName;

    /*
        Generation Task/Request. Will get assigned a unique world/task identifier.
     */
    public Task(double radiusX, double radiusZ, double centerX, double centerZ) {
        this.radiusZ = radiusZ;
        this.radiusX = radiusX;
        this.centerX = centerX;
        this.centerZ = centerZ;
        this.worldName = Utils.generateWorldHASH();
    }
}
