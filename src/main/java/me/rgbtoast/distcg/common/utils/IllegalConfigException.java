package me.rgbtoast.distcg.common.utils;

import lombok.ToString;

@ToString
public class IllegalConfigException extends RuntimeException {

    public IllegalConfigException(String message) {
        super(message);
    }

    public IllegalConfigException(String message, Throwable exception) {
        super(message, exception);
    }

    public IllegalConfigException(Throwable exception) {
        super(exception);
    }

}
